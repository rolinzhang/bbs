package com.rolin.bbs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@RefreshScope
public class NocosConfig {

    @Value("${minio.endpoint}")
    private String endpoint;

    @Value("${minio.endpoint}")
    private String accesskey;

    @Value("${minio.secretKey}")
    private String secretKey;

    @Value("${minio.bucketName}")
    private String bucketName;
}
