package com.rolin.bbs.controller;

import com.rolin.bbs.config.MinioProperties;
import com.rolin.bbs.util.Byte2InputStream;
import com.rolin.bbs.util.RestResult;
import com.rolin.bbs.util.ResultBuilder;
import com.rolin.bbs.util.ResultCodeEnum;
import io.minio.MinioClient;
import io.minio.errors.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.xmlpull.v1.XmlPullParserException;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RestController
public class MinioController {

    private static final Logger log = LoggerFactory.getLogger(MinioController.class);
    @Autowired
    MinioClient minioClient;

    @Autowired
    MinioProperties minioProperties;


    @PostMapping("/makeBucket")
    public RestResult makeBucket(@RequestParam(name="bucketName",required = false)String bucketName ) throws Exception {
            if (bucketName.length()<2||bucketName.length()>63){
             return ResultBuilder.fail(ResultCodeEnum.BUCKET_NAME_ERROR.getCodeMsg());
             }
        boolean flag=minioClient.bucketExists(bucketName) ;
        if (flag!=true){
            minioClient.makeBucket(bucketName);
            log.info("The "+bucketName+" bucket has create success");
            return ResultBuilder.success();
        }else{
            return ResultBuilder.fail(ResultCodeEnum.BUCKET_EXSIT_ERROR.getCodeMsg());
        }
    }



    @PostMapping("file/upload")
    public void upload(@RequestParam(name = "file", required = false) MultipartFile file, HttpServletRequest request) throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, InvalidArgumentException, InvalidResponseException, InternalException, NoResponseException, InvalidBucketNameException, InsufficientDataException, ErrorResponseException {
        InputStream in = file.getInputStream();
        String contentType = file.getContentType();
        minioClient.putObject(minioProperties.getBucketName(), file.getOriginalFilename(), in, null, null, null, contentType);
        log.info(file.getOriginalFilename()+  ResultCodeEnum.FILE_UPLOAD_SUCCESS.getCodeMsg());
    }

    @RequestMapping("/download")
    public ResponseEntity<byte[]> download(String file) throws InvalidPortException, InvalidEndpointException, IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InvalidArgumentException, InvalidResponseException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException {
        byte[] bytes= Byte2InputStream.inputStream2byte(minioClient.getObject(minioProperties.getBucketName(),file));
        HttpHeaders headers = new HttpHeaders();
        String filename = new String(new String(file).getBytes("utf-8"), "iso-8859-1");
        headers.setContentDispositionFormData("attachment", filename);
        headers.setContentLength(bytes.length);
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);

    }

}
