package com.rolin.bbs.util;

public enum ResultCodeEnum implements IResultCode {
    SUCCESS(200, "处理成功"),
    ERROR(500, "服务器内部错误"),
    BUCKET_NAME_ERROR(1000,"操作失败,桶的名称必须在3到63位之间"),
    BUCKET_EXSIT_ERROR(1002,"操作失败,桶的名称已经存在，请更换名字"),
    FILE_UPLOAD_SUCCESS(1003,"文件上传成功"),
    FILE_DOWNLOAD_SUCCESS(1004,"文件下载成功")
    ;

    private int code;

    private String msg;

    ResultCodeEnum(int code, String codeMsg) {
        this.code = code;
        this.msg = codeMsg;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getCodeMsg() {
        return msg;
    }
}
