package com.rolin.bbs.util;

public class ResultBuilder {

    public static RestResult success(){
        RestResult restResult=new RestResult();
        restResult.setCode(ResultCodeEnum.SUCCESS.getCode());
        restResult.setMessage(ResultCodeEnum.SUCCESS.getCodeMsg());
        return restResult;
    }

    public static RestResult success(Object object){
        RestResult restResult=new RestResult();
        restResult.setCode(ResultCodeEnum.SUCCESS.getCode());
        restResult.setMessage(ResultCodeEnum.SUCCESS.getCodeMsg());
        restResult.setData(object);
        return restResult;
    }
    public static RestResult fail(int code ,String message){
        RestResult restResult=new RestResult();
        restResult.setCode(code);
        restResult.setMessage(message);
        return restResult;
    }
    public static RestResult fail(String message){
        RestResult restResult=new RestResult();
        restResult.setMessage(message);
        return restResult;
    }
}
