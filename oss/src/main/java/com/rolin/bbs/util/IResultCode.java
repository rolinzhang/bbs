package com.rolin.bbs.util;

public interface IResultCode {

    int getCode();

    String getCodeMsg();
}
