package com.rolin.bbs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RefreshScope
public class NacosConfig {

    @Value("${spring.cloud.gateway.routes[0].id}")
    private String id;

    @Value("${spring.cloud.gateway.routes[0].uri}")
    private String uri;

    @Value("${spring.cloud.gateway.routes[0].predicates[0]}")
    private String predicates;
    @Value("${spring.cloud.gateway.routes[0].filters[0]}")
    private String filters;

    @Value("${demo-service.ribbon.listOfServers}")
    private String serverList;





}
