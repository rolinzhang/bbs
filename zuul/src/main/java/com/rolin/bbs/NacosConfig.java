package com.rolin.bbs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@RefreshScope
public class NacosConfig {

    @Value("${as.ribbon.listOfServers}")
    private String asServerList;

    @Value("${zuul.routes.user-service.service-id}")
    private String zuulLoadBalanceList;
}
